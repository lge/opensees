#! /bin/sh

SCRIPT=`readlink -f $0`
PROJ_ROOT=`dirname $SCRIPT`
BUILD_DIR=$PWD
MAKES_DIR=$PROJ_ROOT/MAKES

# Guess the system configuration:
SYSTEM=`uname -s`
echo "System: ${SYSTEM}"
if [ $SYSTEM = "Linux" ]; then
    MKDEF_FILE_GUESS=LINUX-CENTOS-6.3-DEBUG
elif [ $SYSTEM = "Drawin"]; then
    MKDEF_FILE_GUESS=MacOS-10.6.8-DEBUG
fi

MKDEF_FILE=${1:-$MKDEF_FILE_GUESS}
echo "Using Makefile.def.${MKDEF_FILE}"

# write to Makefile.def file:
echo "Writing path info to Makefile.def"
cat ${MAKES_DIR}/Makefile.def.${MKDEF_FILE} | \
sed "s|\(BUILD_DIR\s\+\)=.*|\1= $BUILD_DIR|" | \
sed "s|\(PROJECT_ROOT\s\+\)=.*|\1= $PROJ_ROOT|" > $BUILD_DIR/Makefile.def

# copy Makefile tree:
echo "Copying the Makefiles"
cd $PROJ_ROOT
find . \( -name "Makefile*" -o -name "OSver*.sh" \) -not -wholename "*MAKES*" -not -wholename "*BUILD*" | \
cpio -pmdv ${BUILD_DIR} > /dev/null 2>&1

# create build directory
echo "Creating build directories"
mkdir -p ${BUILD_DIR}/lib
mkdir -p ${BUILD_DIR}/bin

echo "Done!"
echo 'Type `make` to build OpenSees, the executable file will be in bin/OpenSees'
