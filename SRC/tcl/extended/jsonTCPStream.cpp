#include <tcl.h>
#include <TCP_Stream.h>
#include <string.h>
#include <unistd.h>
#include "jsonTCPStream.h"

#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>



static TCP_Stream * theTCPStream=0;
int jsonTCPStream(ClientData clientData, Tcl_Interp *interp, int argc,
                  TCL_Char **argv) {
    const char *inetAddr = 0;
    std::string socket_id;
    int inetPort;

    if (argc != 4) {
        return -1;
    } else {
        inetAddr = argv[1];
        socket_id = argv[3];
        if (Tcl_GetInt(interp, argv[2], &inetPort) != TCL_OK) {
            return -1;
        }
    }
    if (theTCPStream == 0) {
        theTCPStream = new TCP_Stream(inetPort, inetAddr);
        // TODO: use sleep() is a dirty way to send socket_id...
        theTCPStream->write(socket_id);
        sleep(1);
    } else {        
        // TODO: do something to change inetAddr, port or socket_id
    }
    return TCL_OK;
};

TCP_Stream * getTheTCPStream(void) {
    return theTCPStream;
}

static int theDomainSocketFd = -1;
int getTheDomainSocketFd(void) {
    return theDomainSocketFd;
}

int writeStrToDomainSocket(const char * cstr) {
    int fd = getTheDomainSocketFd();
    if (fd != -1) {
        write(fd, cstr, strlen(cstr));
        return 0;
    } else {
        return 1;
    }
}

int jsonSetUpDomainSocket(ClientData clientData, Tcl_Interp *interp, int argc,
                          TCL_Char **argv) {
    
    struct sockaddr_un address;
    int  socket_fd, nbytes;
    char buffer[256];
    
    socket_fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if(socket_fd < 0) {
        printf("socket() failed\n");
        return 1;
    }

    /* start with a clean address structure */
    memset(&address, 0, sizeof(struct sockaddr_un));
    
    if (argc != 2) {
        return -1;
    } else {
        snprintf(address.sun_path, DOMAIN_SOCKET_PATH_MAX, argv[1]);        
    }

    address.sun_family = AF_UNIX;
    // snprintf(address.sun_path, UNIX_PATH_MAX, "./demo_socket");

    if(connect(socket_fd, 
               (struct sockaddr *) &address, 
               sizeof(struct sockaddr_un)) != 0) {
        printf("connect() failed\n");
        return 1;
    }
    theDomainSocketFd = socket_fd;
    nbytes = snprintf(buffer, 256, "{\"init\" : \"success!!\"");
    write(socket_fd, buffer, nbytes);
 
    // nbytes = read(socket_fd, buffer, 256);
    // buffer[nbytes] = 0;

    // printf("MESSAGE FROM SERVER: %s\n", buffer);

    // close(socket_fd);

    return 0;
}
