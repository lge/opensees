#ifndef _JSONTCPSTREAM_H_
#define _JSONTCPSTREAM_H_
#include <TCP_Stream.h>
#define DOMAIN_SOCKET_PATH_MAX 100
int jsonTCPStream(ClientData clientData, Tcl_Interp *interp, int argc,
                  TCL_Char **argv);

int jsonSetUpDomainSocket(ClientData clientData, Tcl_Interp *interp, int argc,
                  TCL_Char **argv);

TCP_Stream * getTheTCPStream(void);
int writeStrToDomainSocket(const char * str);

#endif /* _JSONTCPSTREAM_H_ */
