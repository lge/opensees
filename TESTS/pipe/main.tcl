model BasicBuilder -ndm 3 -ndf 3

node 1 1.0 1.0 0.0
node 2 0.0 1.0 0.0
node 3 0.0 0.0 0.0
node 4 1.0 0.0 0.0
node 5 1.0 1.0 1.0
node 6 0.0 1.0 1.0
node 7 0.0 0.0 1.0
node 8 1.0 0.0 1.0

fix 1   0 0 1
fix 2   1 0 1
fix 3   1 1 1
fix 4   0 1 1
#fix 5 1 0 1
#fix 6 1 0 1
#fix 7 1 0 1
#fix 8 1 0 1

# linear elastic material     tag		  E		  nu	   rho
nDMaterial ElasticIsotropic3D 1      100  0.2

#            tag     8 nodes          matID  bforce1 bforce2 bforce3   massdensity
# element Brick8N  1  5 6 7 8 1 2 3 4   1      0.0     0.0     0.0     1.8
element stdBrick  1  5 6 7 8 1 2 3 4   1      0.0     0.0     0.0     1.8
#element stdBrick  1  1 2 3 4 5 6 7 8   1      0.0     0.0    -9.81    1.8
#element stdBrick  1  5 6 7 8 1 2 3 4   1      0.0     0.0    -9.81    1.8

pattern Plain 1 Linear {
    load  5      0    0  -1.0
    load  6      0    0  -1.0 
    load  7      0    0  -1.0 
    load  8      0    0  -1.0 
}

set lf1 1.0

constraints Penalty 1e9 1e9


integrator LoadControl $lf1 

# CHOOSE algorithm
#set different_algorithms  Linear
set different_algorithms  Newton
puts "USING  $different_algorithms ALGORITHM"


# this is needed by Newton 
# test NormDispIncr 1.0e-8 30 1
test NormDispIncr 1.0e-8 30
#test NormUnbalance 1.0e-8 30 1

algorithm $different_algorithms
numberer RCM
system UmfPack
analysis Static

set fd [open "./tmp/opensees.fifo" "w"]
# set fd [open "./tmp/opensees.nonblocking.io.out.txt" "w"]
# fconfigure $fd -blocking 1

proc analyze {args} {
    foreach a $args {
        puts "args $a"
    }              
}

if {true} {
    if {true} {
        for {set i 0} {$i < 10} {incr i} {
            foreach j {a b c} {
                
            }
        }
    } elseif {true} {
        
    } else {
        
    }
}

for {set i 1} {$i <= 5} {incr i} {
    # puts fifo $i
    analyze 1 2 3 54


    #print node 1
    #print node 2
    #print node 3
    #print node 4
    # print node 5
    # puts $fd [nodeDisp 1]
    # puts $fd [nodeDisp 2]
    # puts $fd [nodeDisp 3]
    # puts $fd [nodeDisp 4]
    # puts [nodeDisp 5]
    # puts $fd [eleResponse 1 forces]
    for {set j 1} {$j <= 1} {incr j} {
        puts $fd "i: $i, j: $j, val: [eleResponse 1 material 1 strain]"
    }
    puts "step:$i finished"
    flush $fd
    # puts -nonewline "blocked: "
    # puts [fblocked $fd]
    # puts [eleResponse 1 forces]
    # puts [nodeDisp 5]
    #print node 6
    #print node 7
    #print node 8
    #
    #
    # print -ele 1




}
puts "analyze finished!"
# puts "blocked: [fblocked $fd]"
# flush $fd
# puts "blocked: [fblocked $fd]"
# close $fifo
close $fd
