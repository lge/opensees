model BasicBuilder -ndm 3 -ndf 3

node 1 1.0 1.0 0.0
node 2 0.0 1.0 0.0
node 3 0.0 0.0 0.0
node 4 1.0 0.0 0.0
node 5 1.0 1.0 1.0
node 6 0.0 1.0 1.0
node 7 0.0 0.0 1.0
node 8 1.0 0.0 1.0

mass 3 25 35 54
mass 8 25 35 54

fix 1   0 0 1
fix 2   1 0 1
fix 3   1 1 1
fix 4   0 1 1
#fix 5 1 0 1
#fix 6 1 0 1
#fix 7 1 0 1
#fix 8 1 0 1

# linear elastic material     tag		  E		  nu	   rho
nDMaterial ElasticIsotropic3D 1      100  0.2   1.8

#            tag     8 nodes          matID  bforce1 bforce2 bforce3   massdensity
# element Brick8N  1  5 6 7 8 1 2 3 4   1      0.0     0.0     0.0     1.8
element stdBrick  1  5 6 7 8 1 2 3 4   1      0.0     0.0     0.0     1.8
#element stdBrick  1  1 2 3 4 5 6 7 8   1      0.0     0.0    -9.81    1.8
#element stdBrick  1  5 6 7 8 1 2 3 4   1      0.0     0.0    -9.81    1.8

pattern Plain 1 Linear {
    load  5      0    0  -1.0
    load  6      0    0  -1.0 
    load  7      0    0  -1.0 
    load  8      0    0  -1.0 
}
puts [json-echo-domain]
