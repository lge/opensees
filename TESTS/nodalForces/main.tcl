# cd /home/GL/Develop/server/node-opensees-server/files/shared/projects/truss;
set saved_dir [pwd]
cd /home/GL/Develop/server/node-opensees-server/files/shared/projects/templates/

wipe all;
source frame_23_template.tcl;
puts [json-echo-domain];

recorder Node -file $saved_dir/test.txt -node 1 2 -dof 1 2 3 disp;
set counter 1;
while {$counter < 10} {
    analyze 1 0.02;
    # puts [nodeDisp 1]
    # puts "nodal disp:"
    # puts [json-echo-disp];
    # puts "nodal force:"
    # puts [json-echo-nodal-forces];
    # puts [eleResponse 1 forces]
    incr counter;
}
