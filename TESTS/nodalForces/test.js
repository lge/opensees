var fs = require('fs');
var s1 = fs.createReadStream('./tmp/test.pipe');
s1.on('data', function(data) {
    // console.log(data.toString());
    console.log('receive data of length:', data.length);
    // console.log('receive data of:', data);
    var offset = 0;
    while (offset < data.length) {
        if (offset + 24 > data.length) {
            console.log('offset:', offset, '  rest:', data.slice(offset, data.length).toString());
        } else {
            console.log('offset:', offset, '  number:', data.readDoubleLE(offset));
            console.log('offset:', offset, '  number:', data.readDoubleLE(offset + 8));
            console.log('offset:', offset, '  number:', data.readDoubleLE(offset + 16));
        }
        // console.log('number:', data.readDoubleLE(offset));
        offset += 24;
        
        console.log('offset:', offset, '  stop:', data.readDoubleLE(offset));
        offset += 1;
        // console.log('offset:', offset, '  stop:', data.readDoubleLE(offset));
    }
    // console.log('receive data of:', data.toString());
});